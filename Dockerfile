FROM node:12
#INSTALL DEPENDENCIES
#RUN node --no-warnings 'which autorest'
#RUN npm install -g npm@7.9.0
#RUN npm install -g nodemon
COPY ./package.json ./
RUN npm fund
#RUN npm install cookie-parser
#RUN npm install express
COPY ./ ./
RUN npm install
RUN npm install forever -g
RUN npm audit fix
RUN npm test
# COMMAND
ENTRYPOINT ["npm", "start"]